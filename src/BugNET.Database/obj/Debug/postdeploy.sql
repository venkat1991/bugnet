﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

PRINT 'Adding look up data'
-- Reference Data for BugNet_Permissions 
MERGE INTO BugNet_Permissions AS Target 
USING (VALUES 
  (1, N'CloseIssue', N'Close Issue'),
  (2, N'AddIssue', N'Add Issue'),
  (3, N'AssignIssue', N'Assign Issue'),
  (4, N'EditIssue', N'Edit Issue'),
  (5, N'SubscribeIssue', N'Subscribe Issue'),
  (6, N'DeleteIssue', N'Delete Issue'),
  (7, N'AddComment', N'Add Comment'),
  (8, N'EditComment', N'Edit Comment'),
  (9, N'DeleteComment', N'Delete Comment'),
  (10, N'AddAttachment', N'Add Attachment'),
  (11, N'DeleteAttachment', N'Delete Attachment'),
  (12, N'AddRelated', N'Add Related Issue'),
  (13, N'DeleteRelated', N'Delete Related Issue'),
  (14, N'ReopenIssue', N'Re-Open Issue'),
  (15, N'OwnerEditComment', N'Edit Own Comments'),
  (16, N'EditIssueDescription', N'Edit Issue Description'),
  (17, N'EditIssueTitle', N'Edit Issue Title'),
  (18, N'AdminEditProject', N'Admin Edit Project'),
  (19, N'AddTimeEntry', N'Add Time Entry'),
  (20, N'DeleteTimeEntry', N'Delete Time Entry'),
  (21, N'AdminCreateProject', N'Admin Create Project'),
  (22, N'AddQuery', N'Add Query'),
  (23, N'DeleteQuery', N'Delete Query'),
  (24, N'AdminCloneProject', N'Clone Project'),
  (25, N'AddSubIssue', N'Add Sub Issues'),
  (26, N'DeleteSubIssue', N'Delete Sub Issues'),
  (27, N'AddParentIssue', N'Add Parent Issues'),
  (28, N'DeleteParentIssue', N'Delete Parent Issues'),
  (29, N'AdminDeleteProject', N'Delete a project'),
  (30, N'ViewProjectCalendar', N'View the project calendar'),
  (31, N'ChangeIssueStatus', N'Change an issues status field'),
  (32, N'EditQuery', N'Edit queries')
) 
AS Source (PermissionId, PermissionKey, PermissionName) 
ON Target.PermissionId = Source.PermissionId 
-- update matched rows 
WHEN MATCHED THEN 
UPDATE SET PermissionKey = Source.PermissionKey, PermissionName = Source.PermissionName
-- insert new rows 
WHEN NOT MATCHED BY TARGET THEN 
INSERT (PermissionId,  PermissionKey, PermissionName) 
VALUES (PermissionId,  PermissionKey, PermissionName) 
-- delete rows that are in the target but not the source 
WHEN NOT MATCHED BY SOURCE THEN 
DELETE;

-- Reference Data for BugNet_ProjectCustomFieldTypes
SET IDENTITY_INSERT BugNet_ProjectCustomFieldTypes ON 
GO

MERGE INTO BugNet_ProjectCustomFieldTypes AS Target 
USING (VALUES 
   (1, N'Text'),
   (2, N'Drop Down List'),
   (3, N'Date'),
   (4, N'Rich Text'),
   (5, N'Yes / No'),
   (6, N'User List')
) 
AS Source (CustomFieldTypeId, CustomFieldTypeName) 
ON Target.CustomFieldTypeId = Source.CustomFieldTypeId
-- update matched rows 
WHEN MATCHED THEN 
UPDATE SET CustomFieldTypeName = Source.CustomFieldTypeName 
-- insert new rows 
WHEN NOT MATCHED BY TARGET THEN 
INSERT (CustomFieldTypeId, CustomFieldTypeName) 
VALUES (CustomFieldTypeId, CustomFieldTypeName) 
-- delete rows that are in the target but not the source 
WHEN NOT MATCHED BY SOURCE THEN 
DELETE;

SET IDENTITY_INSERT BugNet_ProjectCustomFieldTypes OFF 
GO




-- Reference Data for BugNet_RequiredFieldList 
MERGE INTO BugNet_RequiredFieldList AS Target 
USING (VALUES 
	(1, N'-- Select Field --', N'0'),
	(2, N'Issue Id', N'IssueId'),
	(3, N'Title', N'IssueTitle'),
	(4, N'Description', N'IssueDescription'),
	(5, N'Type', N'IssueTypeId'),
	(6, N'Category', N'IssueCategoryId'),
	(7, N'Priority', N'IssuePriorityId'),
	(8, N'Milestone', N'IssueMilestoneId'),
	(9, N'Status', N'IssueStatusId'),
	(10, N'Assigned', N'IssueAssignedUserId'),
	(11, N'Owner', N'IssueOwnerUserId'),
	(12, N'Creator', N'IssueCreatorUserId'),
	(13, N'Date Created', N'DateCreated'),
	(14, N'Last Update', N'LastUpdate'),
	(15, N'Resolution', N'IssueResolutionId'),
	(16, N'Affected Milestone', N'IssueAffectedMilestoneId'),
	(17, N'Due Date', N'IssueDueDate'),
	(18, N'Progress', N'IssueProgress'),
	(19, N'Estimation', N'IssueEstimation'),
	(20, N'Time Logged', N'TimeLogged'),
	(21, N'Custom Fields', N'CustomFieldName')
) 
AS Source (RequiredFieldId, FieldName, FieldValue) 
ON Target.RequiredFieldId = Source.RequiredFieldId
-- update matched rows 
WHEN MATCHED THEN 
UPDATE SET FieldName = Source.FieldName , FieldValue = Source.FieldValue
-- insert new rows 
WHEN NOT MATCHED BY TARGET THEN 
INSERT (RequiredFieldId, FieldName, FieldValue) 
VALUES (RequiredFieldId, FieldName, FieldValue) 
-- delete rows that are in the target but not the source 
WHEN NOT MATCHED BY SOURCE THEN 
DELETE;

-- Reference Data for BugNet_Languages 
MERGE INTO BugNet_Languages AS Target 
USING (VALUES 
	('en-US', 'English (United States)', 'en'),
	('es-ES', 'Spanish (Spain)', 'en-US'),
	('nl-NL', 'Dutch (Netherlands)', 'en-US'),
	('it-IT', 'Italian (Italy)', 'en-US'),
	('ru-RU', 'Russian (Russia)', 'en-US'),
	('ro-RO', 'Romanian (Romania)', 'en-US'),
	('fr-CA', 'French (Canadian)', 'en-US'),
	('de-DE', 'German (Germany)', 'en-US')
) 
AS Source ([CultureCode], [CultureName], [FallbackCulture]) 
ON Target.CultureCode = Source.CultureCode
-- update matched rows 
WHEN MATCHED THEN 
UPDATE SET CultureName = Source.CultureName, FallbackCulture = Source.FallbackCulture
-- insert new rows 
WHEN NOT MATCHED BY TARGET THEN 
INSERT (CultureCode, CultureName, FallbackCulture) 
VALUES (CultureCode, CultureName, FallbackCulture) 
-- delete rows that are in the target but not the source 
WHEN NOT MATCHED BY SOURCE THEN 
DELETE;

-- Reference Data for BugNet_HostSettings
MERGE INTO BugNet_HostSettings AS Target 
USING (VALUES 
	(N'AdminNotificationUsername', N'admin'),
	(N'ADPassword', N''),
	(N'ADPath', N''),
	(N'ADUserName', N''),
	(N'AllowedFileExtensions', N'*.*'),
	(N'ApplicationTitle', N'BugNET Issue Tracker'),
	(N'DefaultUrl', N'http://localhost/BugNet/'),
	(N'AnonymousAccess', N'False'),
	(N'UserRegistration', N'0'),
	(N'EmailErrors', N'False'),
	(N'EnableRepositoryCreation', N'True'),
	(N'ErrorLoggingEmailAddress', N'myemail@mysmtpserver.com'),
	(N'FileSizeLimit', N'2024'),
	(N'HostEmailAddress', N'noreply@mysmtpserver.com'),
	(N'Pop3BodyTemplate', N'templates/NewMailboxIssue.xslt'),
	(N'Pop3DeleteAllMessages', N'False'),
	(N'Pop3InlineAttachedPictures', N'False'),
	(N'Pop3Interval', N'6000'),
	(N'Pop3Password', N''),
	(N'Pop3ReaderEnabled', N'True'),
	(N'Pop3ReportingUsername', N'Admin'),
	(N'Pop3Server', N''),
	(N'Pop3Username', N'bugnetuser'),
	(N'Pop3UseSSL', N'False'),
	(N'RepositoryBackupPath', N''),
	(N'RepositoryRootPath', N'C:\\SVN\\'),
	(N'RepositoryRootUrl', N'svn://localhost/'),
	(N'SMTPAuthentication', N'False'),
	(N'SMTPPassword', N''),
	(N'SMTPPort', N'25'),
	(N'SMTPServer', N'localhost'),
	(N'SMTPUsername', N''),
	(N'SMTPUseSSL', N'False'),
	(N'SvnAdminEmailAddress', N''),
	(N'SvnHookPath', N''),
	(N'UserAccountSource', N'None'),
	(N'Version', N''),
	(N'WelcomeMessage', N'Welcome to your new BugNET installation! Log in as <br/><br/> Username: admin <br/>Password: password'),
	(N'OpenIdAuthentication', N'False'),
	(N'SMTPEmailTemplateRoot', N'~/templates'),
	(N'SMTPEMailFormat', N'2'),
	(N'SMTPDomain', N''),
	(N'ApplicationDefaultLanguage', N'en-US'),
	(N'Pop3ProcessAttachments', N'False'),
	(N'EnableGravatar', N'False'),
	(N'GoogleAuthentication', N'False'),
	(N'GoogleClientId', N''),
	(N'GoogleClientSecret', N''),
	(N'FacebookAuthentication', N'False'),
	(N'FacebookAppId', N''),
	(N'FacebookAppSecret', N''),
	(N'TwitterAuthentication', N'False'),
	(N'TwitterConsumerKey', N''),
	(N'TwitterConsumerSecret', N''),
	(N'MicrosoftAuthentication', N'False'),
	(N'MicrosoftClientId', N''),
	(N'MicrosoftClientSecret', N'')
) 
AS Source ([SettingName], [SettingValue]) 
ON Target.SettingName = Source.SettingName
-- update matched rows 
--WHEN MATCHED THEN 
--UPDATE SET SettingName = Source.SettingName, SettingValue = Source.SettingValue
-- insert new rows 
WHEN NOT MATCHED BY TARGET THEN 
INSERT (SettingName, SettingValue) 
VALUES (SettingName, SettingValue) 
-- delete rows that are in the target but not the source 
WHEN NOT MATCHED BY SOURCE THEN 
DELETE;

SET IDENTITY_INSERT BugNet_Roles ON 
GO
-- Reference Data for BugNet_Languages 
MERGE INTO BugNet_Roles AS Target 
USING (VALUES 
	(1, NULL, N'Super Users', N'A role for application super users', 0)
) 
AS Source ([RoleId], [ProjectId], [RoleName], [RoleDescription], [AutoAssign]) 
ON Target.RoleId = Source.RoleId

-- insert new rows 
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([RoleId], [ProjectId], [RoleName], [RoleDescription], [AutoAssign]) 
VALUES ([RoleId], [ProjectId], [RoleName], [RoleDescription], [AutoAssign]); 

SET IDENTITY_INSERT BugNet_Roles OFF 
GO
